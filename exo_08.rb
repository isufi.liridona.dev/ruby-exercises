# Écris un programme exo_08.rb qui demande un nombre à l'utilisateur, puis qui affiche un compte à rebours à partir de ce nombre, jusqu'à 0.

puts "Give me a number: "
n = gets.to_i

def countdown(n)   
    if n == 0
        puts n
    else
        puts n
        countdown(n - 1)
    end
end 

countdown(n)  
