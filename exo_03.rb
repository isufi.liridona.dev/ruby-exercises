# Écris un programme exo_03.rb qui demande son année de naissance à l'utilisateur, puis qui ressort l'âge que l'utilisateur a eu en 2017.

def subtraction_function
    puts "Quel est votre année de naissance ?"
    n1 = gets.to_i
    n2 = Time.now.year
    n3 = n2 - n1
    answer = n3 - 4
    puts "Vous aviez #{answer} ans en 2017"
end

subtraction_function()