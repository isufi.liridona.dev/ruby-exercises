# Écris un programme exo_06.rb qui demande un nombre à un utilisateur, puis qui écrit autant de fois -1 "Bonjour toi !".
# Ainsi, si l'utilisateur rentre 10, le programme devra écrire 9 fois "Bonjour toi !"

def subtraction_function()
    puts "Choisissez un nombre entre 1 et 10 !"
    
    n = gets.to_i

    x =  n - 1
    
    x.times { 
        puts "Bonjour, toi !"
    }

end

subtraction_function()
