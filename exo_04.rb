# Écris un programme exo_04.rb qui demande son année de naissance à l'utilisateur, puis qui ressort l'année où l'utilisateur aura 100 ans.

def subtraction_function()
    puts "Quel est votre année de naissance ?"
    #user's answer
    n1 = gets.to_i

    #current year
    n2 = Time.now.year

    #current age
    n3 = n2 - n1

    n4 = 100

    #years needed for user to be 100 (100 - current age)
    n5 = n4 - n3

    #year when user will be 100
    answer = n2 + n5
  
    puts "Vous aurez 100 ans en #{answer}"
end

subtraction_function()
